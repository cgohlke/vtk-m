# Delete deprecated features

With the major release of VTK-m 2.0, we are cleaning up the code by
removing deprecated features. For software using VTK-m 1.9, you should have
gotten deprecation warnings about any feature that is removed. Thus, to
ease porting to VTK-m 2.0, software should consider updating to VTK-m 1.9
first.
